### Train_rk_yolo8
* 作者：北小菜 
* 官网：http://www.beixiaocai.com
* 邮箱：bilibili_bxc@126.com
* QQ：1402990689
* 微信：bilibili_bxc
* 哔哩哔哩主页：https://space.bilibili.com/487906612
* gitee开源地址：https://gitee.com/Vanishi/BXC_AutoML
* github开源地址：https://github.com/beixiaocai/BXC_AutoML


### 基础环境
| 程序         | 版本               |
| ---------- |------------------|
| python     | 3.8+             |
| 依赖库      | requirements.txt |

### 特别特别注意！！！
* 请不要从yolo8的github官方仓库下载代码！！！
* 本模块的代码来自于 https://github.com/airockchip/ultralytics_yolov8
* https://github.com/airockchip/ultralytics_yolov8 的代码来自于 https://github.com/ultralytics/ultralytics (8.2.82)

### 虚拟环境安装依赖库

~~~

//创建虚拟环境
python -m venv venv

//windows系统激活虚拟环境
venv\Scripts\activate

//linux系统激活虚拟环境
source venv/bin/activate

//安装依赖（本仓库的安装依赖toml文件，并不是传统requirements.txt的方式，请注意区别）

//第1步
python -m ensurepip --default-pip
//第2步
pip install poetry
//第3步
pip install .

//（如果电脑有英伟达显卡，并且安装了显卡驱动，可以安装cuda版pytorch用于训练加速）
pip install torch==2.1.0 torchaudio==2.1.0 torchvision==0.16.0 --index-url https://download.pytorch.org/whl/cu121

~~~


### 训练模型
* rk版yolo8训练模型和普通yolo8训练模型是完全一样的操作，所以如需rk版yolo8训练模型，请参考文档：https://gitee.com/Vanishi/BXC_AutoML/blob/master/Train_yolo8/README.md


### 模型转换：pt模型->onnx模型

~~~

//windows系统安装依赖库（建议python3.10，其他python的依赖库版本可能会有所不同，也可以根据报错自行修改版本）：
pip install onnxruntime==1.19.0 onnx==1.16.1  -i https://pypi.tuna.tsinghua.edu.cn/simple

//linux系统依赖库（建议python3.8，其他python的依赖库版本可能会有所不同，也可以根据报错自行修改版本）：
pip install onnxruntime==1.16.3 onnx==1.16.1  -i https://pypi.tuna.tsinghua.edu.cn/simple

//最最最重要的一步，将pt模型转换为rk设备支持的onnx模型
yolo export model=yolov8n.pt format=rknn

~~~


### 模型转换：onnx模型->rknn模型

~~~
//将rk设备支持的onnx转换为rknn模型（参考文档）
https://gitee.com/Vanishi/BXC_AutoML/tree/master/onnx2rknn

~~~
