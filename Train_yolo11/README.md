### Train_yolo11
* 作者：北小菜 
* 官网：http://www.beixiaocai.com
* 邮箱：bilibili_bxc@126.com
* QQ：1402990689
* 微信：bilibili_bxc
* 哔哩哔哩主页：https://space.bilibili.com/487906612
* gitee开源地址：https://gitee.com/Vanishi/BXC_AutoML
* github开源地址：https://github.com/beixiaocai/BXC_AutoML

### 基础环境
| 程序         | 版本               |
| ---------- |------------------|
| python     | 3.8+             |

### 安装Python（推荐Python3.8或者之后的版本）
* [python-官网下载地址](https://www.python.org/getit/)
* [python-夸克网盘下载地址](https://pan.quark.cn/s/72df133d1343)


### 安装环境请注意
* 在使用python开发项目时，推荐使用python的虚拟环境，因为同一台电脑上很可能会安装多个python项目，而不同的python项目可能会使用不同的依赖库，为了避免依赖库不同而导致的冲突，强烈建议使用python虚拟环境
* 关于如何使用python虚拟环境，非常简单，文档最下面提供Windows系统和Linux系统创建和使用虚拟环境的方法


### Windows系统安装 pytorch-cpu版本yolo11
* pip install ultralytics==8.3.1 -i https://pypi.tuna.tsinghua.edu.cn/simple
* pip install torch==2.1.2 torchvision==0.16.2 -i https://pypi.tuna.tsinghua.edu.cn/simple

### Windows系统安装 pytorch-cuda版本yolo11
* pip install ultralytics==8.3.1 -i https://pypi.tuna.tsinghua.edu.cn/simple
* pip install torch==2.1.0 torchaudio==2.1.0 torchvision==0.16.0 --index-url https://download.pytorch.org/whl/cu121
* 注意：安装pytorch-gpu训练环境，请根据自己的电脑硬件选择cuda版本，比如我上面选择的https://download.pytorch.org/whl/cu121，并非适用所有电脑设备，请根据自己的设备选择


### 快速开始
~~~
//查看已安装的yolo11版本
yolo -V

//训练检测模型（gpu版本）
yolo detect train model=yolov8n.pt data=/datasets/data.yaml batch=64 epochs=5 imgsz=640 device=cuda
（注意：关于/datasets/data.yaml文件，是指向训练数据集的配置文件，如果不清楚可以先下载一份数据集参考下）

//训练检测模型（cpu版本）
yolo detect train model=yolov8n.pt data=/datasets/data.yaml batch=64 epochs=5 imgsz=640 device=cpu

//测试模型
yolo detect predict model=runs/train/best.pt source=test.jpg

//将pt模型转换为onnx格式模型
//依赖库：pip install onnxruntime==1.19.0 onnx==1.16.1  -i https://pypi.tuna.tsinghua.edu.cn/simple
yolo export model=best.pt format=onnx

//将pt模型转换为openvino格式模型
//依赖库：pip install openvino==2024.3.0 openvino-dev==2024.3.0 onnxruntime==1.19.0 onnx==1.16.1  -i https://pypi.tuna.tsinghua.edu.cn/simple
yolo export model=best.pt format=openvino

~~~


### mo命令将pt模型转换为openvino模型（方式二）
* mo命令是openvino官方提供的模型转换工具
* mo参考文档 https://blog.csdn.net/qq_44632658/article/details/131270531
~~~
//安装mo命令行，将onnx转换为openvino模型
//依赖库：pip install openvino==2024.3.0 openvino-dev==2024.3.0 onnxruntime==1.19.0 onnx==1.16.1  -i https://pypi.tuna.tsinghua.edu.cn/simple
yolo export model=yolov8n.pt format=onnx
mo --input_model yolov8n.onnx  --output_dir yolov8n_openvino_model
~~~

### trtexec命令将pt模型转换为tensorrt模型
~~~
//将pt模型转换为onnx格式模型
yolo export model=yolov8n.pt format=onnx

//将onnx模型转换为tensorrt格式模型(在ovtrt版本的视频行为分析系统的xcms_core文件夹下面有trtexec工具)
trtexec --onnx=yolov8n.onnx --saveEngine=yolov8n.fp16.engine --fp16
~~~

### yolo11训练相关文档
* yolo11官方训练参数：https://docs.ultralytics.com/zh/modes/train/#train-settings
* yolo11开源地址：https://github.com/ultralytics/ultralytics

### yolo11训练模型推荐数据集（数据集来自闲鱼商家，需要的用户可以前往购买）

* 【闲鱼】[https://m.tb.cn/h.TfTfzjj?tk=1fJl3xAWOQR](https://m.tb.cn/h.TfTfzjj?tk=1fJl3xAWOQR)【抽烟检测数据集（共计7375张）】」
* 【闲鱼】[https://m.tb.cn/h.TUbJRRQ?tk=HjE23xA4p4l](https://m.tb.cn/h.TUbJRRQ?tk=HjE23xA4p4l)【反光衣检测数据集（共计1083张）】」
* 【闲鱼】[https://m.tb.cn/h.TfoLXjr?tk=vBP23xAUPbf](https://m.tb.cn/h.TfoLXjr?tk=vBP23xAUPbf)【火焰烟雾检测数据集（共计2119张）】」
* 【闲鱼】[https://m.tb.cn/h.TfThtgE?tk=MHWq3xAfjwj](https://m.tb.cn/h.TfThtgE?tk=MHWq3xAfjwj)【人头、安全帽检测数据集（共计11592张）】」
* 【闲鱼】[https://m.tb.cn/h.TfoLraA?tk=5l003xAUTyH](https://m.tb.cn/h.TfoLraA?tk=5l003xAUTyH)【学生课堂行为数据集。数据包含3类学生课堂常见行为：举手,看书】」
* 【闲鱼】[https://m.tb.cn/h.TfT7SWh?tk=GdHk3xA52La](https://m.tb.cn/h.TfT7SWh?tk=GdHk3xA52La)【打架检测数据集（共计2831张）】」
* 【闲鱼】[https://m.tb.cn/h.Tf4AyLZ?tk=6bSr3xA598g](https://m.tb.cn/h.Tf4AyLZ?tk=6bSr3xA598g)【粉尘检测数据集（共计3833张）】」
* 【闲鱼】[https://m.tb.cn/h.TfTijoq?tk=bFBQ3xAgdGi](https://m.tb.cn/h.TfTijoq?tk=bFBQ3xAgdGi)【睡岗检测数据集（共计2123张）】」
* 【闲鱼】[https://m.tb.cn/h.TfTi03n?tk=grPd3xA5vM2](https://m.tb.cn/h.TfTi03n?tk=grPd3xA5vM2)【站立，摔倒，坐，深蹲，跑5个人体动作数据集（共计5422张）】」

### Windows系统安装Python虚拟环境
~~~

//创建虚拟环境
python -m venv venv

//切换到虚拟环境
venv\Scripts\activate

//更新虚拟环境的pip版本（可以不更新）
python -m pip install --upgrade pip -i https://pypi.tuna.tsinghua.edu.cn/simple

~~~

### Linux系统安装Python虚拟环境

~~~
//创建虚拟环境
python -m venv venv

//激活虚拟环境
source venv/bin/activate

//更新虚拟环境的pip版本（可以不更新）
python -m pip install --upgrade pip -i https://pypi.tuna.tsinghua.edu.cn/simple

~~~



