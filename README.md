### BXC_AutoML
* 作者：北小菜 
* 官网：http://www.beixiaocai.com
* 邮箱：bilibili_bxc@126.com
* QQ：1402990689
* 微信：bilibili_bxc
* 哔哩哔哩主页：https://space.bilibili.com/487906612
* gitee开源地址：https://gitee.com/Vanishi/BXC_AutoML
* github开源地址：https://github.com/beixiaocai/BXC_AutoML

### 介绍
* Train_yolo8: yolo8官方训练框架
* Train_yolo11: yolo11官方训练框架
* Train_rk_yolo5: 适用于瑞芯微设备的yolo5模型训练框架
* Train_rk_yolo8: 适用于瑞芯微设备的yolo8模型训练框架
* Train_rk_yolo11: 适用于瑞芯微设备的yolo11模型训练框架
* onnx2rknn: 适用于瑞芯微设备的onnx模型转换为rknn模型工具
* Train_ResNet: 基于resnet的图片分类算法训练框架
* Train_CnnLstm: 基于cnn+lstm的视频分类算法训练框架
* labeltools: 样本转换脚本

### 相关视频教程
* [训练第1讲，介绍算法训练工具](https://www.bilibili.com/video/BV1Em421s7ep)
* [训练第2讲，介绍收集样本/labelme标注样本/自动将标注的样本转换为yolo格式的训练数据集](https://www.bilibili.com/video/BV1cBHYekESt)
* [训练第3讲，介绍训练模型](https://www.bilibili.com/video/BV17WtjezEoQ)

### 相关工具
* [视频分割图片工具 hs](https://gitee.com/Vanishi/BXC_hs)
* [互联网图片样本下载工具 DownloadImage](https://gitee.com/Vanishi/BXC_DownloadImage)
* [图片标注样本工具 labelme](https://pan.quark.cn/s/7e6accce2a3e)
* [xcms](https://gitee.com/Vanishi/xcms)

### 更新记录
#### 2025/01/06
* 新增Train_rk_yolo11
* onnx2rknn更新至rk2.3.0，并支持arm和x86两种架构

#### 2024/12/16
* 新增Train_yolo11
* 更新Train_yolo8/Train_yolo11/Train_ResNet推荐数据集链接，增加了更为优质的训练数据集链接

#### 2024/11/24 
* 新增适用于瑞芯微设备的yolo8模型训练框架Train_rk_yolo8
* 优化Train_rk_yolo5的使用说明
* 优化onnx2rknn的使用说明

#### 2024/9/11 
* 解决Train_yolo8将pt转onnx时错误问题，原因是onnx库的版本问题，在Train_yolo8/README.md文档已经设置了具体的版本
#### 2024/8/31 
* 解决Train_yolo8使用pip install ultralytics时错误问题，原因是python版本问题，在Train_yolo8/README.md文档已经设置了具体的版本
#### 2024/8/6
* （1）新增样本标注工具labelme的辅助脚本工具，可以将label标注的样本转化为yolo检测格式样本，或转化为resnet分类格式样本
* （2）新增检测格式样本和分类格式样本自动按照比例分割成训练集和测试集的脚本工具
#### 2024/7/30
* 新增基于cnn+lstm网络结构的视频分类算法训练框架，Train_CnnLstm
#### 2024/4/27 
* 首次上传