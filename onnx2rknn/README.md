### onnx模型转换为rknn模型

### 介绍
* onnx转rknn支持在arm-linux或x86-linux上进行
* linux系统推荐Ubuntu20，因为作者提供的依赖库全部都是基于Python3.8，Ubuntu20默认Python就是3.8
* 特别特别特别特别注意：rk设备用到的onnx模型，是非常特殊的，请务必使用作者提供的yolo5/yolo8/yolo11训练仓库训练pt模型并转rk支持的onnx

### 安装arm架构依赖环境

| 程序         | 版本               |
| ---------- |------------------|
| 架构     | arm             |
| python     | 3.8             |
| rknn_toolkit     | 2.3.0             |
| 依赖库      | 3rdparty-arm/arm64_requirements_cp38.txt |
| 依赖库      | 3rdparty-arm/rknn_toolkit2-2.3.0-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl |

### 安装x86架构依赖环境

| 程序         | 版本               |
| ---------- |------------------|
| 架构     | x86             |
| python     | 3.8             |
| rknn_toolkit     | 2.3.0             |
| 依赖库      | 3rdparty-x86/requirements_cp38-2.3.0.txt |
| 依赖库      | 3rdparty-x86/rknn_toolkit2-2.3.0-cp38-cp38-manylinux_2_17_x86_64.manylinux2014_x86_64.whl |

### 训练支持rk的模型
* 特别注意：请使用下面的仓库进行训练（rk用到的onnx模型都是经过特殊处理的，请务必使用下面的代码）

* 训练rk版yolo5模型请参考：https://gitee.com/Vanishi/BXC_AutoML/tree/master/Train_rk_yolo5
* 训练rk版yolo8模型请参考：https://gitee.com/Vanishi/BXC_AutoML/tree/master/Train_rk_yolo8
* 训练rk版yolo8模型请参考：https://gitee.com/Vanishi/BXC_AutoML/tree/master/Train_rk_yolo11


### 安装依赖库
~~~

//创建虚拟环境
python -m venv venv

//切换到虚拟环境
source venv/bin/activate

//更新pip
python -m pip install --upgrade pip -i https://pypi.tuna.tsinghua.edu.cn/simple

//安装arm架构依赖环境（第一步）特别注意：非常不建议使用代理，如 -i https://pypi.tuna.tsinghua.edu.cn/simple
python -m pip install --retries 60  -r 3rdparty-arm/arm64_requirements_cp38.txt

//安装arm架构依赖环境（第二步）
pip install 3rdparty-arm/rknn_toolkit2-2.3.0-cp38-cp38-manylinux_2_17_aarch64.manylinux2014_aarch64.whl


//安装x86架构依赖环境（第一步）特别注意：非常不建议使用代理，如 -i https://pypi.tuna.tsinghua.edu.cn/simple
python -m pip install --retries 60  -r 3rdparty-x86/requirements_cp38-2.3.0.txt

//安装x86架构依赖环境（第二步）
pip install 3rdparty-x86/rknn_toolkit2-2.3.0-cp38-cp38-manylinux_2_17_x86_64.manylinux2014_x86_64.whl


~~~


### 如何使用
* 将yolo5/yolo8/yolo11训练的onnx转换为rknn
* python onnx2rknn_Yolo.py 
